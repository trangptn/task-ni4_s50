const express = require("express");
const cors = require("cors");

require("dotenv").config();

const db = require("./app/models");

const { initial } = require("./data");

const app = express();

app.use(cors());

app.use(express.json());

const bodyParser=require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));

const path=require('path');

app.use(express.static(__dirname+'/app/views'));

/*
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));*/

db.mongoose
    .connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`)
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial();
    })
    .catch((err) => {
        console.error("Connection error", err);
        process.exit();
    })

    app.get("/", (req, res) => {
        console.log(__dirname);
        res.sendFile(path.join(__dirname+ '/app/views/login.html'));
    });

app.use("/api/auth/", require("./app/routes/auth.routes"));
app.use("/api/user/", require("./app/routes/user.routes"));

const PORT = process.env.ENV_PORT || 7000;

app.listen(PORT, () => {
    console.log("App listening on port:", PORT);
})

