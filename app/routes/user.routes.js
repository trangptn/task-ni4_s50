const express = require("express");

const route = express.Router();

const userMiddleWare = require("../middlewares/user.middleware");
const userController = require("../controllers/user.controller");

route.get("/",userMiddleWare.authenticateToken,userController.getAllUser);
route.get("/getRole",userController.getRole);
route.post("/", userMiddleWare.authenticateToken, userController.createUser);
route.get("/:id",userMiddleWare.authenticateToken,userController.getUserById);
route.put("/:id", userMiddleWare.authenticateToken, userController.updateUser);
route.delete("/:id", userMiddleWare.authenticateToken, userController.deleteUser);

module.exports = route;