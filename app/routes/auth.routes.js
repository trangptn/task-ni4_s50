const express = require("express");

const route = express.Router();

const authMiddleWare = require("../middlewares/auth.middleware");
const authController = require("../controllers/auth.controller");

// API signup chỉ dành cho user 
route.post("/signup", authMiddleWare.checkDuplicateUsername, authController.signUp);

route.post("/login", authController.logIn);

route.post("/refreshToken",authController.refreshToken);

route.post("/refresh-token",authController.refresh);

module.exports = route;