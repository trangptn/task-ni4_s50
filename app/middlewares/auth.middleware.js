const db = require("../models");
const jwt = require('jsonwebtoken');

const User = db.user;
const Role = db.role;
const ROLES = db.ROLES;

const checkDuplicateUsername = async (req, res, next) => {
    try {
        const existUser = await User.findOne({
            username: req.body.username
        });

        if(existUser) {
            res.status(400).send({
                message: "Username is already in use"
            })

            return;
        }

        next();
    } catch (error) {
        console.error("Interal server error", error);
        process.exit();   
    }
}

module.exports = {
    checkDuplicateUsername
}