const db = require("../models");
const jwt = require('jsonwebtoken');

const User = db.user;

const verifyToken = async (req, res, next) => {
    const authHeader=req.headers['authorization'];

    const token=authHeader && authHeader.split('  ')[1];
    console.log(token);
    if (!token) {
        return res.status(401).send({
            message: "Không tìm thấy Authorization!"
        });
    }

    const secretKey = process.env.JWT_SECRET;

    const verified=jwt.verify(token,process.env.JWT_SECRET,(err,decoded)=>{
        if(err)
            {
                return res.status(403).json({
                    message:`Failed to authenticate token ${err.message}`
                })
            }
            return decoded;
    })
    console.log(verified);
    if (!verified) {
        return res.status(401).send({
            message: "Authorization không hợp lệ!"
        });
    }

    const user = await User.findById(verified.id).populate("roles");
    req.user = user;
    next();    
}


const checkUser = (req, res, next) => {
    console.log("check user ...");
    const userRoles = req.user.roles;
    if (userRoles) {
        for (let i=0; i<userRoles.length; i++) {
            if (userRoles[i].name == 'admin' ) {
                console.log("Authorized!");
                next();
                return;
            }
        }
    }

    console.log("Unauthorized");
    return res.status(401).send({
        message: "Bạn không có quyền truy cập!"
    });

    /*
    User.findById(req.userId)
    .then((data) => {
        console.log("verify ok");
        console.log(data);
        Role.find({_id : {$in : data.roles}})
        .then((roles) => {    
            console.log(roles);
            for (let i=0; i<roles.length; i++) {
                if (roles[i].name == 'admin' ) {
                    console.log("authorized!");
                    next();
                    return;
                }
            }
            res.status(401).send({
                message: "Unauthorized!"
            })
            return;
        })
        .catch((err) => {
            res.status(401).send({
                message: "Unauthorized!"
            })
            return;
        })

    })
    .catch((err) => {
        console.log("verify nok");
        console.log(err);
        return;
    })
    
    /*
    .exec((err, user) => {
        if (err) {
            res.status(500).send({ message: err });
            return;
        }
        console.log("verify role:");
        console.log(user);
    })*/
}

const authenticateToken=(req,res,next)=>{
    const authHeader=req.headers['authorization'];

    const token=authHeader && authHeader.split('  ')[1];

    const secretKey = process.env.JWT_SECRET;

    if(!token)
        {
            return res.status(401).send("Unauthorized");
        }

    jwt.verify(token,secretKey,(err,user)=>{
        if(err)
            {
                if(err.name==="TokenExpiredError")
                    {
                        return res.status(401).send("Token has expired");
                    }
                else
                {
                    return res.status(403).send("Fobiden")
                }
            }
        req.user=user;
        next();
          
    })
}

module.exports = {
    verifyToken,
    checkUser,
    authenticateToken
}