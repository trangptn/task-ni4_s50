const db=require("../models");
const mongoose=require("mongoose");
const bcrypt=require('bcrypt');

const userModel=db.user;

const getAllUser = async(req, res) => {
try{
    const result=await userModel.find();
    return res.status(200).json({
        message: "Get All Users",
        data:result
    })

}
catch(err)
{
    console.log(err);
    return res.status(500).json({
        message:"Invalid error "
    })
}
   
}

const createUser = async(req, res) => {
    const{
        reqUsername,
        reqPassword,
        reqRole
    }=req.body
    if(!reqUsername)
        {
            return res.status(400).json({
                message:"Username invalid !"
            })
        }
    if(!reqUsername){
        return res.status(400).json({
            message:"username invalid !"
        })
    }
    if(!reqRole)
        {
            return res.status(400).json({
                message:"Role invalid!"
            })
        }
    try
    {
        const result=await userModel.create({
            username:reqUsername,
            password: bcrypt.hashSync(reqPassword, 8),
            roles:[reqRole]
        })

        return res.status(201).json({
            message:"Create user successfully",
            data:result
        })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Invalid error!"
        })
    }
}

const getUserById=async(req,res)=>{
    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id))
        {
            return res.status(400).json({
                message:"Id invalid"
            })
        }
    try{
        const result=await userModel.findById(_id);
        if(result)
            {
                return res.status(200).json({
                    message:"Successful",
                    data:result
                })
            }
        else{
            return res.status(404).json({
                message:"Not found user!"
            })
        }
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Invalid error!"
        })
    }
}

const updateUser = async(req, res) => {
    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id))
        {
            return res.status(400).json({
                message:"Id invalid"
            })
        }
    const {username,role}=req.body;
    const password=bcrypt.hashSync(req.body.password,8);
    if( username===""){
        return res.status(400).json({
            message:"Username invalid !"
        })
    }

    if( password==="")
        {
           return  res.status(400).json({
                message:"Password invalid !"
            })
        }
        if( role==="")
            {
               return  res.status(400).json({
                    message:"Role invalid !"
                })
            }
    try
    {
        let updateUser={};
        if(username){
            updateUser.username=username;
        }
        if(password){
            updateUser.password=password;
        }
        if(role)
        {
            updateUser.roles=role;
        }
        const result= await userModel.findByIdAndUpdate(_id,updateUser);
        if(result)
            {
                return res.status(200).json({
                    message:"Update successfull !",
                    data:result
                })
            }
        else{
            return res.status(400).json({
                message:"Couldn't update product type !"
            })
        }
    }
    catch(err)
    {
        console.log(err);
        return res.status(500).json({
            message:"Invalid error"
        })
    }
}

const deleteUser = async (req, res) => {

    const _id=req.params.id;
    if(!mongoose.Types.ObjectId.isValid(_id)){
        return res.status(400).json({
            message:"Id invalid !"
        })
    }

    try{
        const result=await userModel.findByIdAndRemove(_id);
        if(result){
            return res.status(200).json({
                message:"Delete successful !"
            })
        }
        else
        {
            return res.status(404).json({
                message:"Couldn't find product"
            })
        }
        
    }
    catch(error)
    {
        return res.status(500).json({
            message:"Invalid error"
        })
    }
}
const getRole=async(req,res)=>{
    try{
        const result=await db.role.find();
        res.status(201).json({
            data:result
        })
    }
    catch(err)
    {
        console.log(err);
    }
}

module.exports = {
    getAllUser,
    createUser,
    updateUser,
    deleteUser,
    getUserById,
    getRole
}